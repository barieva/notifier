<%@ page contentType="text/html; charset=UTF-8" %>

<!doctype html>
<html lang="en">
<head>
    <title>Регистрация</title>

    <%--    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/checkout/">--%>

    <!-- Bootstrap core CSS -->
    <link href="../../css/min.css" rel="stylesheet"
          crossorigin="anonymous">
</head>

<body class="bg-light">
<div class="container">
    <div class="py-5 text-center">
        <h1>Регистрация</h1>
    </div>
    <div class="">
        <div class="col-md-auto">
            <h4 class="mb-3">Заполните поля</h4>
            <form id="form" class="needs-validation"
                  action="${pageContext.request.contextPath}/register"
                  modelAttribute="account" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">Имя</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="${user.name}"
                               name="name" required>
                        <div class="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">Email</label>
                        <input type="text" class="form-control" id="email" placeholder="" value="${user.email}"
                               name="email" required>
                        <p style="color:red"> ${message}</p>
                        <div class="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="password">Пароль</label>
                    <input type="password" class="form-control" id="password" placeholder="пароль"
                           name="password" required>
                    <div class="invalid-feedback">
                        Please enter your password.
                    </div>
                </div>
                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Зарегистрируйте меня</button>
            </form>
        </div>
    </div>
    <footer class="my-5 pt-5 text-muted text-center text-small">
        от <a href="http://www.getjavajob.com/">getJavaJob</a>
    </footer>
</div>
</body>
</html>