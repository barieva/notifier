<%@ page contentType="text/html; charset=UTF-8" %>
<meta charset="UTF-8">
<!doctype html>
<html lang="en">
<head>
    <title>Sign in</title>
    <%--    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">--%>
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/min.css" rel="stylesheet"
          crossorigin="anonymous">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet">
</head>
<body class="text-center">
<form class="form-signin" action="/" method="post">
    <h1 class="h3 mb-3 font-weight-normal">Выполните вход</h1>
    <p style="color:red"> ${message}</p>
    ${param.error}
    <label for="email" class="sr-only">Эл. адрес</label>
    <input type="email" id="email" class="form-control" placeholder="Эл. адрес" name="email" value="${email}"
           required autofocus>
    <label for="password" class="sr-only">Пароль</label>
    <input type="password" id="password" class="form-control" placeholder="Пароль" name="password" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Вход</button>
    <a class="btn btn-lg btn-outline-success btn-block" type="submit"
       href="${pageContext.request.contextPath}/registration">Регистрация</a>
    <p class="mt-5 mb-3 text-muted">от <a href="http://www.getjavajob.com/">getJavaJob</a></p>
</form>
</body>
</html>