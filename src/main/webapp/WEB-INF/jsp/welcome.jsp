<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <link href="${pageContext.request.contextPath}/css/min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/classic.css" rel="stylesheet">

</head>
<body class="bg-light">
Добро пожаловать, ${sessionScope.user.name}!
<h5><a class="badge badge-primary"
       href="${pageContext.request.contextPath}/logout">Выйти<span
        class="sr-only">(current)</span></a>
</h5>

</body>
</html>