package com.exam.controller;

import com.exam.domain.Message;
import com.exam.domain.User;
import com.exam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import static com.exam.domain.Action.REGISTRATION;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class RegisterController {
    private UserService userService;
    private JmsTemplate jmsTemplate;

    @Autowired
    public RegisterController(UserService userService, JmsTemplate jmsTemplate) {
        this.userService = userService;
        this.jmsTemplate = jmsTemplate;
    }

    @PostMapping("/register")
    public ModelAndView register(@ModelAttribute User user) {
        ModelAndView modelAndView = new ModelAndView();
        if (userService.isUserExistsByEmail(user.getEmail())) {
            setValuesForForwardingToSamePage(user, modelAndView, "/registration");
        } else {
            userService.save(user);
            Message message = createMessageAsRegistered(user);
            jmsTemplate.convertAndSend("jms", message);
            modelAndView.setViewName("redirect:login");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/", method = {GET, POST})
    public String login() {
        return "login";
    }

    private void setValuesForForwardingToSamePage(User user, ModelAndView modelAndView, String url) {
        modelAndView.addObject("user", user);
        modelAndView.addObject("message", "Такой email существует");
        modelAndView.setViewName(url);
    }

    private Message createMessageAsRegistered(User user) {
        Message message = new Message();
        message.setUser(user);
        message.setSubject(REGISTRATION);
        message.setText(user.getName() + " registered. Write him by: " + user.getEmail());
        return message;
    }
}