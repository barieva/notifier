package com.exam.dao;

import com.exam.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, Integer> {
    User findByEmail(String email);

    boolean existsByEmail(String email);
}