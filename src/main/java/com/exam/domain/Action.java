package com.exam.domain;

public enum Action {
    REGISTRATION("User registered"), LOGGING_IN("User logged in"), LOGGING_OUT("User logged out");
    String description;

    Action(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}