package com.exam.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document
@Getter
@Setter
public class User implements Serializable {
    @Id
    private String id;
    @Indexed(unique = true)
    private String email;
    private String password;
    private String name;
}