package com.exam.service;

import com.exam.domain.Message;
import com.exam.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageSender {
    private MessageService messageService;
    private UserService userService;

    @Autowired
    public MessageSender(MessageService messageService, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @JmsListener(destination = "jms")
    public void notifyUsers(Message message) {
        List<User> users = userService.getUsers();
        String producedActionEmail = message.getUser().getEmail();
        users.parallelStream().
                filter(user -> !user.getEmail().equals(producedActionEmail)).forEach(user -> {
            messageService.send(message, user.getEmail());
        });
    }
}