package com.exam.service;

import com.exam.dao.UserRepository;
import com.exam.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.security.crypto.bcrypt.BCrypt.gensalt;
import static org.springframework.security.crypto.bcrypt.BCrypt.hashpw;

@Service
public class UserService {
    public static final int TEN_ROUNDS = 10;
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(User user) {
        makePasswordAsHashed(user);
        userRepository.save(user);
    }

    public boolean isUserExistsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    private void makePasswordAsHashed(User user) {
        String hashedPassword = hashpw(user.getPassword(), gensalt(TEN_ROUNDS));
        user.setPassword(hashedPassword);
    }
}
