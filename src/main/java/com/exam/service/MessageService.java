package com.exam.service;

import com.exam.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
    private JavaMailSender javaMailSender;
    @Value("spring.mail.username")
    private String senderMail;

    @Autowired
    public MessageService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void send(Message message, String receiverEmail) throws MailException {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(receiverEmail);
        mailMessage.setFrom(senderMail);
        mailMessage.setSubject(message.getSubject().getDescription());
        mailMessage.setText(message.getText());
        javaMailSender.send(mailMessage);
    }

}
