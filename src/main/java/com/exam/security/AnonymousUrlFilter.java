package com.exam.security;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

public class AnonymousUrlFilter extends GenericFilterBean {
    private Set<String> anonymousUrls = new HashSet<>(asList("/", "/register", "/registration"));

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (isAnonymousUrl((HttpServletRequest) servletRequest) && isAuthenticated()) {
            ((HttpServletResponse) servletResponse).sendRedirect("/welcome");
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private boolean isAuthenticated() {
        return getContext().getAuthentication() != null
                && getContext().getAuthentication().isAuthenticated();
    }

    private boolean isAnonymousUrl(HttpServletRequest servletRequest) {
        String requestURI = servletRequest.getRequestURI();
        return anonymousUrls.contains(requestURI);
    }
}