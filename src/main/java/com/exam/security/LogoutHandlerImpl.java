package com.exam.security;

import com.exam.domain.Message;
import com.exam.domain.User;
import com.exam.domain.UserPrincipal;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.exam.domain.Action.LOGGING_OUT;

@Component
public class LogoutHandlerImpl implements LogoutHandler {
    private JmsTemplate jmsTemplate;

    @Autowired
    public LogoutHandlerImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @SneakyThrows
    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        User user = ((UserPrincipal) authentication.getPrincipal()).getUser();
        Message message = createMessageAsLoggedOut(user);
        jmsTemplate.convertAndSend("jms", message);
        httpServletResponse.sendRedirect("/");
    }

    private Message createMessageAsLoggedOut(User user) {
        Message message = new Message();
        message.setUser(user);
        message.setSubject(LOGGING_OUT);
        message.setText(user.getName() + " logged out.");
        return message;
    }
}

