package com.exam.security;

import com.exam.domain.Message;
import com.exam.domain.User;
import com.exam.domain.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.exam.domain.Action.LOGGING_IN;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {
    private HttpSession session;
    private JmsTemplate jmsTemplate;

    @Autowired
    public AuthenticationSuccessHandlerImpl(HttpSession session, JmsTemplate jmsTemplate) {
        this.session = session;
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {
        User user = ((UserPrincipal) authentication.getPrincipal()).getUser();
        session.setAttribute("user", user);
        httpServletResponse.sendRedirect("/welcome");
        Message message = createMessageAsLoggedIn(user);
        jmsTemplate.convertAndSend("jms", message);
    }

    private Message createMessageAsLoggedIn(User user) {
        Message message = new Message();
        message.setUser(user);
        message.setSubject(LOGGING_IN);
        message.setText(user.getName() + " logged in. Write him by: " + user.getEmail());
        return message;
    }
}