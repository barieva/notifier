package com.exam.dao;

import com.exam.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    @Test
    public void testSavingUser() {
        User user = initUser();
        userRepository.save(user);
        assertEquals(user.getEmail(), userRepository.findAll().get(0).getEmail());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSavingNullUser() {
        userRepository.save(null);
    }

    @Test()
    public void testEmailExists() {
        User user = initUser();
        userRepository.save(user);
        assertTrue(userRepository.existsByEmail(user.getEmail()));
    }

    @Test()
    public void testEmailDoNotExists() {
        assertFalse(userRepository.existsByEmail("Nonexistent"));
    }

    @Test()
    public void testFindAll() {
        User user = initUser();
        User user2 = initUser();
        user2.setEmail("other");
        userRepository.save(user);
        userRepository.save(user2);
        assertEquals(2, userRepository.findAll().size());
    }

    private User initUser() {
        User user = new User();
        user.setName("Bob");
        user.setEmail("1@gmail.com");
        user.setPassword("1");
        return user;
    }
}

