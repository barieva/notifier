package com.exam.service;

import com.exam.dao.UserRepository;
import com.exam.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.LinkedList;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;
    @InjectMocks
    private UserService userService;

    private static User initUser(int id) {
        User account = new User();
        account.setId(id + "");
        account.setName("Anny");
        account.setEmail("any@mail.com");
        account.setPassword("$2a$10$DUIZbD/j7v6rBgkEXkaZfOoW5Y2ROCHRP7oS60qNqhQeEyUsJtgBy");
        return account;
    }

    @Test
    public void testCreatingUser() {
        User user = initUser(2);
        userService.save(user);
    }

    @Test
    public void testUserExistsByEmail() {
        User user = initUser(2);
        when(userRepository.existsByEmail(user.getEmail())).thenReturn(true);
        assertTrue(userService.isUserExistsByEmail(user.getEmail()));
    }

    @Test
    public void testUserDoNotExistsByEmail() {
        User user = initUser(2);
        when(userRepository.existsByEmail(user.getEmail())).thenReturn(false);
        assertFalse(userService.isUserExistsByEmail(user.getEmail()));
    }

    @Test
    public void testGetAllUsers() {
        User user = initUser(2);
        User secondUser = initUser(3);
        when(userRepository.findAll()).thenReturn(asList(user, secondUser));
        Collection<User> expected = new LinkedList<>();
        expected.add(user);
        expected.add(secondUser);
        assertEquals(expected, userService.getUsers());
    }
}